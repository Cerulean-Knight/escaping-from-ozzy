﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour {

    public int high;
    public Text AltoPuntaje;
    
	// Use this for initialization
	void Start () {
        high = PlayerPrefs.GetInt("Highscore");
        AltoPuntaje = gameObject.GetComponent<Text>();
        AltoPuntaje.text = "Highscore: " + high;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
