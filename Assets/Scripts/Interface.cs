﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Interface : MonoBehaviour {
    public float restartDelay = 2f;

    Animator anim;
    private bool requestedRestart = false;
    private float restartTimer;

    public bool inicio;
    public float inicioDealy = 2f;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    void Start()
    {
        Cursor.visible = false;

        inicio = false;

        StartCoroutine(Inicio());
    }

    void Update()
    {
        if (requestedRestart)
        {
            restartTimer += Time.deltaTime;
            if (restartTimer >= restartDelay)
                SceneManager.LoadScene("Game");
        }

        if (Input.GetKeyDown(KeyCode.Escape))// || Input.GetKeyDown("joystick 6 button"))
            SceneManager.LoadScene("MainMenu");
    }

    IEnumerator Inicio()
    {
        yield return new WaitForSeconds(inicioDealy);

        anim.SetBool("Inicio", true);

        inicio = true;
    }

    public void GameOver(string text)
    {
        if (requestedRestart)
            return;

        requestedRestart = true;
        restartTimer = 0f;
        Text gameOverText = GameObject.Find("TextoGameOver").GetComponent<Text>();
        gameOverText.text = text;
        anim.SetTrigger("GameOver");
    }
}
