﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public float MouseSensitivityX;
    public float MouseSensitivityY;

    public float JoySensitivityX;
    public float JoySensitivityY;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    public float minimumZ = -2f;
    public float maximumZ = 2f;

    float rotationX = 0F;
    float rotationY = 0F;

    private List<float> rotArrayX = new List<float>();
    float rotAverageX = 0F;

    private List<float> rotArrayY = new List<float>();
    float rotAverageY = 0F;

    float rotAverageZ = 0f;

    public float frameCounter = 20;

    Quaternion originalRotation;

    public float velocidad = 10f;
    public float suavidad = 10f;

    public Interface inter;
    private Player player;

    void Awake()
    {
        player = GetComponent<Player>();
        if (inter == null)
            Debug.LogError("Reference to Interface missing");
    }

    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();		
        originalRotation = rb.rotation;
    }

    void FixedUpdate()
    {
        if (inter.inicio && player.alive)
        {
            rotAverageY = 0f;
            rotAverageX = 0f;

            rotationY += Input.GetAxis("Mouse Y") * MouseSensitivityY;
            rotationX += Input.GetAxis("Mouse X") * MouseSensitivityX;

            rotationY += Input.GetAxis("Vertical") * JoySensitivityY;
            rotationX += Input.GetAxis("Horizontal") * JoySensitivityX;

            rotArrayY.Add(rotationY);
            rotArrayX.Add(rotationX);

            if (rotArrayY.Count >= frameCounter)
            {
                rotArrayY.RemoveAt(0);
            }
            if (rotArrayX.Count >= frameCounter)
            {
                rotArrayX.RemoveAt(0);
            }

            for (int j = 0; j < rotArrayY.Count; j++)
            {
                rotAverageY += rotArrayY[j];
            }
            for (int i = 0; i < rotArrayX.Count; i++)
            {
                rotAverageX += rotArrayX[i];
            }

            rotAverageY /= rotArrayY.Count;
            rotAverageX /= rotArrayX.Count;

            rotAverageY = ClampAngle(rotAverageY, minimumY, maximumY);
            rotAverageX = ClampAngle(rotAverageX, minimumX, maximumX);
            rotAverageZ = ClampAngle(-rotAverageX, minimumZ, maximumZ);

            Quaternion yQuaternion = Quaternion.AngleAxis(rotAverageY, Vector3.left);
            Quaternion xQuaternion = Quaternion.AngleAxis(rotAverageX, Vector3.up);
            Quaternion zQuaternion = Quaternion.AngleAxis(rotAverageZ, Vector3.forward);

            rb.rotation = Quaternion.Slerp(rb.rotation, originalRotation * xQuaternion * yQuaternion * zQuaternion, suavidad);

            transform.Translate(transform.forward * velocidad * Time.deltaTime, Space.World);

        }
        else if (inter.inicio && !player.alive)
        {
            rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }
        return Mathf.Clamp(angle, min, max);
    }
}
