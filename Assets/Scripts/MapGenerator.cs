﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

    public GameObject firstBlock;
    public GameObject restBlock;
    public List<GameObject> blocks = new List<GameObject>();
    public List<GameObject> childsUp = new List<GameObject>();
    public List<GameObject> childsDown = new List<GameObject>();
	public List<GameObject> childsAnims = new List<GameObject>();
    
    public int multiplicador = 100;
    public int mapSize = 30;
    public float randomX = 0.2f;
    public int childModulus = 2;
    public int childSize = 1;
    public int maxChildSize = 15;

    private Vector3 currentPosition = new Vector3(0f, 0f, 0f);
    private GameObject currentRestBlock = null;
    public void RegenerateMap()
    {
        CleanMap();
        GenerateMap();
    }
    GameObject InsertBlock(GameObject blockPrefab, bool generateChilds)
    {
        GameObject obj = (GameObject)Instantiate(blockPrefab, currentPosition, Quaternion.identity, transform);
        Block block = obj.GetComponent<Block>();
            currentPosition += block.translation * multiplicador;
        if (generateChilds)
            GenerateChildrens(obj, block.translation);
        return obj;
    }

    void GenerateChildrens(GameObject padre, Vector3 translation)
	{	
        Vector3 pos = Vector3.zero;

		int maxRandom = 70;

		if (padre.name == firstBlock.name + "(Clone)")
			maxRandom = 100;
		
        for (int i = 0; i < childSize / childModulus; i++)
        {
			int selectedBlock = 0;
			GameObject child = null;
			int upDown = Random.Range(0, maxRandom);
				
			if (upDown <= 40) { // Down

				selectedBlock = Random.Range (0, childsDown.Count);
				child = childsDown [selectedBlock];

			} else if (upDown > 40 && upDown <= 70) { // Up

				selectedBlock = Random.Range (0, childsUp.Count);

				child = childsUp [selectedBlock];
			}
			else if (upDown > 70) { // Anims
				
				selectedBlock = Random.Range (0, childsAnims.Count);

				child = childsAnims [selectedBlock];
		}

			GameObject obj = (GameObject)Instantiate(child, Vector3.zero, Quaternion.identity, padre.transform);
            
			Vector3 randomPos = new Vector3 (Random.Range (-randomX, randomX), 0f, 0f);

			obj.transform.localPosition = pos + randomPos;
            pos += (translation * multiplicador) / (childSize / childModulus);
        }
    }

    void CleanMap()
    {
       // Debug.LogWarning("Cleaning up map");
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            if (obj != currentRestBlock)
                Destroy(obj);
        }
    }

    void GenerateMap()
    {
        InsertBlock(firstBlock, false);

        for (int i = 0; i < mapSize; i++)
        {
            int selectedBlock = Random.Range(0, blocks.Count);
            InsertBlock(blocks[selectedBlock], true);
        }

        currentRestBlock = InsertBlock(restBlock, false);
    }

    public void LevelUp()
    {
        if (childSize < maxChildSize)
            childSize++;
    }

    void Start()
    {
        GenerateMap();
    }
}
