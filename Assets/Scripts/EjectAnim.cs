﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjectAnim : MonoBehaviour {

	//public float dealy = 5f;

	Animator anim;

	void Awake () {
	
		anim = transform.GetChild(0).GetComponent<Animator>();

	}

	void Animar()
	{
		anim.SetTrigger ("Animar");
	}

	private void OnTriggerEnter(Collider other)
	{
		Animar();
	}

}
