﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public bool alive;
    public Interface inter;
    public PlayMusic playMusic;

	// Use this for initialization
	void Start () {
        alive = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void kill(string text = "Game Over")
    {
        inter.GameOver(text);
        alive = false;
    }

    private void OnTriggerEnter(Collider other)
    {
		if (other.name != "peace" && other.name != "caos" && other.name != "AnimVigas" && other.name != "AnimVigas(Clone)" && other.name != "AnimCarro" && other.name != "AnimCarro(Clone)")
        {
            playMusic.PlayGameOver();
            kill();
        }
            
    }
}
