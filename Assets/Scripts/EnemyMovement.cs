﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public float speed,maxSpeed;
    public int aceleracion;
    public GameObject objetivo;
    private Vector3 objetivoDirection;

    public void SlowDown()
    {
        speed /= 2;
    }
    void OnTriggerEnter(Collider co) {
        if (co.name == objetivo.GetComponent<Collider>().name)
            objetivo.GetComponent<Player>().kill("Te agarro Ozzy, wacho");
    }

    void Update() {
        objetivoDirection = objetivo.transform.position - transform.position;
        GetComponent<Rigidbody>().velocity = objetivoDirection.normalized * speed;
        if (speed < maxSpeed){
            speed += Time.deltaTime * aceleracion;
        }
    }
}
