﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rest : MonoBehaviour {
    public EnemyMovement enemy;
    public MapGenerator map;
    public PlayerMovement playerMovement;

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "peace") {
            enemy.SlowDown();
            playerMovement.velocidad += 5;
            enemy.speed += 5;
            enemy.maxSpeed += 5;
            map.LevelUp();
            map.RegenerateMap();
        }
    }
}
