﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCursor : MonoBehaviour {
    public Texture2D cursorTexture;
	// Use this for initialization
	void Start () {
        Cursor.SetCursor(cursorTexture, new Vector2(100, 0), CursorMode.Auto);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
