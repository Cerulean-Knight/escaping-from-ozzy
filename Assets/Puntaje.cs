﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puntaje : MonoBehaviour
{
    int number = 0;
    Text puntaje;
    float secondsCounter = 0;
    float secondsToCount = 1;
    

    void Start(){
        puntaje = gameObject.GetComponent<Text>();
    }
    
    void Update()
    {
        secondsCounter += Time.deltaTime;
        if (secondsCounter >= secondsToCount){
            secondsCounter = 0;
            number++;
        }
        puntaje.text = "Score: " + number * 10;
    }
}