﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private int number = 0;
    private Text puntaje;
    private float secondsCounter = 0;
    private float secondsToCount = 1;
    public HighScore highscore;

    void Start()
    {
        puntaje = gameObject.GetComponent<Text>();
    }

    void Update()
    {
        secondsCounter += Time.deltaTime;
        if (secondsCounter >= secondsToCount)
        {
            secondsCounter = 0;
            number++;
        }
        puntaje.text = "Score: " + number * 10;
        if (number*10 > highscore.high)
        {
            highscore.AltoPuntaje.text = "Highscore: " + number * 10;
            PlayerPrefs.SetInt("Highscore", number*10);
        }
    }
}